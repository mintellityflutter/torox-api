CREATE DATABASE torox_test;

USE torox_test;

CREATE TABLE IF NOT EXISTS `client` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(128) NOT NULL,
    `created_at` DATE,
    `is_active` BOOLEAN
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_role` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

INSERT INTO
    user_role (id, name)
VALUES
    (1, 'sa'),
    (2, 'client_admin'),
    (3, 'inv_manager'),
    (4, 'electrician_qualified'),
    (5, 'electrician');

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `test_command` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `code` VARCHAR(255) NOT NULL
);

INSERT INTO
    test_command (id, name, code)
VALUES
    (1, 'Phasenlage', 'bi'),
    (2, 'Hochspannung-P', 'hp119'),
    (3, 'Hochspannung-N', 'hn119'),
    (4, 'Gleichspannung', 'pe02dc01'),
    (5, 'Wechselspannung', 'pe02ac01');

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `role_id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `has_premium` BOOLEAN DEFAULT 0,
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(role_id) REFERENCES user_role(id),
    FOREIGN KEY(client_id) REFERENCES client(id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `customer` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `company` VARCHAR(255),
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `customer_location` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `customer_id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `address` VARCHAR(255),
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(customer_id, client_id) REFERENCES customer(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `location_room` (
    `client_id` INT NOT NULL,
    `id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255),
    `location_id` INT NOT NULL,
    PRIMARY KEY(client_id, id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(client_id, location_id) REFERENCES customer_location(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventory_list` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `customer_location_id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255),
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(customer_location_id, client_id) REFERENCES customer_location(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `test_process` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255),
    `deprecated_since` DATE,
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `test_step` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `test_process_id` INT NOT NULL,
    `test_command_id` INT NOT NULL,
    `order` INT NOT NULL,
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(test_command_id) REFERENCES test_command(id),
    FOREIGN KEY(test_process_id, client_id) REFERENCES test_process(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `device` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `inventory_list_id` INT NOT NULL,
    `room_id` INT,
    `name` VARCHAR(255) NOT NULL,
    `number` VARCHAR(255),
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(inventory_list_id, client_id) REFERENCES inventory_list(id, client_id),
    FOREIGN KEY(client_id, room_id) REFERENCES location_room(client_id, id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `test` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `device_id` INT NOT NULL,
    `finished_at` DATE,
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(device_id, client_id) REFERENCES device(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `test_protocol` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `test_id` INT NOT NULL,
    `test_step_id` INT NOT NULL,
    `value` VARCHAR(255),
    `has_passed` BOOLEAN NOT NULL,
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(test_id, client_id) REFERENCES test(id, client_id),
    FOREIGN KEY(test_step_id, client_id) REFERENCES test_step(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `test_list` (
    `id` INT NOT NULL,
    `client_id` INT NOT NULL,
    `start_at` DATE NOT NULL,
    PRIMARY KEY(id, client_id),
    FOREIGN KEY(client_id) REFERENCES client(id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS device_testprocess_map (
    client_id INT NOT NULL,
    device_id INT NOT NULL,
    test_process_id INT NOT NULL,
    PRIMARY KEY(client_id, device_id, test_process_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(device_id, client_id) REFERENCES device(id, client_id),
    FOREIGN KEY(test_process_id, client_id) REFERENCES test_process(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS user_testlist_map (
    client_id INT NOT NULL,
    user_id INT NOT NULL,
    test_list_id INT NOT NULL,
    PRIMARY KEY(client_id, user_id, test_list_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(user_id, client_id) REFERENCES user(id, client_id),
    FOREIGN KEY(test_list_id, client_id) REFERENCES test_list(id, client_id)
);

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS device_testlist_map (
    client_id INT NOT NULL,
    device_id INT NOT NULL,
    test_list_id INT NOT NULL,
    PRIMARY KEY(client_id, device_id, test_list_id),
    FOREIGN KEY(client_id) REFERENCES client(id),
    FOREIGN KEY(device_id, client_id) REFERENCES device(id, client_id),
    FOREIGN KEY(test_list_id, client_id) REFERENCES test_list(id, client_id)
);

-- --------------------------------------------------------

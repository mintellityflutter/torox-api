const db = require('/opt/nodejs/db-handler');
const resp = require('/opt/nodejs/response');
const dbConn = require('/opt/nodejs/db-connection');
const vandium = require('vandium');

exports.handler = vandium.api()
    .before(async _ => await db.connect(dbConn))
    .GET(
        async _ => db.getClients().then(res => resp(200, res)))
    .PUT({
        body: { name: 'string:min=1,max=128,required' }
    },
        async event => db.updateClient(event.pathParameters.id, { name: event.body.name })
            .then(_ => resp(200, {}))
            .catch(_ => resp(400, {})))
    .POST({
        body: { name: 'string:min=1,max=128,required' }
    },
        async event => db.addClient(event.body.name).then(res => resp(200, res)))
    .finally(_ => db.close());

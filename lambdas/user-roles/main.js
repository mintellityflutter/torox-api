const db = require('/opt/nodejs/db-handler');
const resp = require('/opt/nodejs/response');
const dbConn = require('/opt/nodejs/db-connection');
const vandium = require('vandium');

exports.handler = vandium.api()
    .before(async _ => await db.connect(dbConn))
    .GET(
        async () => {
            var res = await db.getUserRoles();

            return resp(200, res);
        })
    .finally(_ => {
        db.close();
    });

const mysql = require('promise-mysql');
const utils = require('./utils');

class DbHandler {
    addClient(name) {
        return this.addRow('client', {
            name,
            is_active: true,
            created_at: utils.formatDate(new Date())
        });
    }

    getClient(id) {
        return this.getRow('client', { id });
    }

    getClients() {
        return this.conn.query('SELECT * FROM client');
    }

    updateClient(id, data) {
        return this.updateRow('client', data, {
            id
        });
    }

    getUserRoles() {
        return this.conn.query('SELECT * FROM user_role');
    }

    //#region helpers

    connect(cfg) {
        return mysql.createConnection(cfg).then(c => this.conn = c);
    }

    close() {
        return this.conn.end();
    }

    getRow(table, cond) {
        const sql = `SELECT * FROM ${table} WHERE ${this.getDividedKeysString(cond, ' AND ')}`;

        return this.conn.query(sql, Object.values(cond))
            .then(res => res && res[0] ? res[0] : null);
    }

    addRow(table, data) {
        const keys = Object.keys(data);
        const keyString = keys.reduce((acc, key, i) => acc + `${key}${i == keys.length - 1 ? '' : ','}`, '');
        const valString = keys.reduce((acc, key, i) => acc + `?${i == keys.length - 1 ? '' : ','}`, '');
        const queryString = `INSERT INTO ${table} (${keyString}) VALUES (${valString})`;

        return this.conn.query(queryString, Object.values(data))
            .then(res => {
                data.id = res.insertId;

                return data;
            });
    }

    delRow(table, cond) {
        const query = `DELETE FROM ${table} WHERE ${this.getDividedKeysString(cond, ' AND ')}`;

        return this.conn.query(query, Object.values(cond))
            .then(res => res.affectedRows > 0 ? res : Promise.reject());
    }

    updateRow(table, data, cond) {
        const keysString = (keys, sep) => keys.reduce((acc, key, i) => acc + `${key}=?${i == keys.length - 1 ? '' : sep}`, '');
        const queryParams = Object.values(data).concat(Object.values(cond));
        const queryString = `UPDATE ${table} SET ${keysString(Object.keys(data), ', ')} WHERE ${keysString(Object.keys(cond), ' AND ')}`;

        return this.conn.query(queryString, queryParams)
            .then(res => res.affectedRows > 0 ? res : Promise.reject());
    }

    getDividedKeysString(obj, divider = ',') {
        const keys = Object.keys(obj);

        return keys.reduce((acc, key, i) => acc + `${key}=?${i == keys.length - 1 ? '' : divider}`, '');
    }

    //#endregion
}

module.exports = new DbHandler();

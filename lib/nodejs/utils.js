class Utils {
    constructor() {}

    pad(src, size) {
        var s = String(src);
        while (s.length < (size || 2)) s = '0' + s;

        return s;
    }

    formatDate(date) {
        var y = date.getFullYear();
        var m = this.pad(date.getMonth() + 1, 2);
        var d = this.pad(date.getDate(), 2);

        var th = this.pad(date.getHours(), 2);
        var tm = this.pad(date.getMinutes(), 2);
        var ts = this.pad(date.getSeconds(), 2);

        return `${y}-${m}-${d} ${th}:${tm}:${ts}`;
    }
}

module.exports = new Utils();

module.exports = async (code, body) => {
    var res = {
        body: JSON.stringify(body),
        statusCode: code,
        headers: {
            "Access-Control-Allow-Origin": '*',
            "Content-Type": 'application/json'
        }
    };

    return res;
};

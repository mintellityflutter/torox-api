const chai = require('chai');
chai.use(require('chai-http'));

const timeout = 15000;
const exp = chai.expect;
const api = 'http://127.0.0.1:3000';

describe('api', function () {
    this.timeout(timeout);

    describe('user-roles', function () {
        it('[GET] /user-roles -> [200]', async () => {
            const res = await chai.request(api).get('/user-roles');

            exp(res).to.have.status(200);
            exp(res.body).to.be.an('array');
            exp(res.body.length).to.eq(5);
        });
    });

    describe('clients', function () {
        it('[GET] /clients -> [200]', async () => {
            const res = await chai.request(api).get('/clients');

            exp(res).to.have.status(200);
            exp(res.body).to.be.an('array');
        });

        it('[POST] /clients -> no input [400]', async () => {
            const res = await chai.request(api).post('/clients');

            exp(res).to.have.status(400);
            exp(res.body.type).to.eq('ValidationError');
            exp(res.body.message).to.eq('"value" must be of type object');
        });

        it('[POST] /clients -> "name" required [400]', async () => {
            const res = await chai.request(api).post('/clients')
                .send({});

            exp(res).to.have.status(400);
            exp(res.body.type).to.eq('ValidationError');
            exp(res.body.message).to.eq('"name" is required');
        });

        it('[POST] /clients -> "name" is empty [400]', async () => {
            const res = await chai.request(api).post('/clients')
                .send({
                    name: ''
                });

            exp(res).to.have.status(400);
            exp(res.body.type).to.eq('ValidationError');
            exp(res.body.message).to.eq('"name" is not allowed to be empty');
        });

        it('[POST] /clients -> [200]', async () => {
            const res = await chai.request(api).post('/clients')
                .send({
                    name: 'test-client'
                });

            exp(res).to.have.status(200);
            exp(res.body).to.be.an('object');
            exp(res.body.id).that.is.a('number');
            exp(res.body.name).to.eq('test-client');
        });
    });
});

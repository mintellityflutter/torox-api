const db = require('../lib/nodejs/db-handler');
const exp = require('chai').expect;
const dbCfg = require('../lib/nodejs/db-connection');

describe('db-handler.js', () => {
    after(async () => await db.close());
    before(async () => await db.connect(dbCfg));

    describe('clients', () => {
        it('getClients()', async () => {
            const res = await db.getClients();

            exp(res).to.be.an('array');
        });

        it('addClient()', async () => {
            const res = await db.addClient('test');

            exp(res).to.be.an('object');
        });

        it.only('updateClient()', async () => {
            const old = await db.addClient('need update');
            
            await db.updateClient(old.id, {
                name: 'updated'
            });

            const updated = await db.getClient(old.id);

            exp(updated).to.be.an('object');
            exp(updated.name).to.eq('updated');
        });
    });

    describe('userRoles', () => {
        it('getUserRoles() -> roles', async () => {
            const res = await db.getUserRoles();

            exp(res).to.be.an('array');
            exp(res.length).to.eq(5);
        });
    });
});
